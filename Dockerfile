FROM alpine

# RUN buildDeps=' \
#               npm \
#       ' \
#       && apk --no-cache add --update \
#       $buildDeps \
#       && apk del --purge $buildDeps

RUN apk add --update npm

WORKDIR /app

COPY package.json ./

RUN npm install

COPY . ./

CMD ["npm", "start"]
