# To build
`docker build -t node-server:optimized .`

# To run for test
`docker run --rm -p 8080:8080 -m 40000001 node-server:optimized`